# OpenML dataset: Harry-Potter-fanfiction-data

https://www.openml.org/d/43544

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Huge Harry Potter fan. Wanted to collect fan-fiction data to make a dashboard and visualize it. Its in the works. 
Content
I scraped this data from https://www.fanfiction.net/book/Harry-Potter/ using requests and beautiful soup. The data is completely structured. The scraping code can be found at https://github.com/nt03/HarryPotter_fanfics/tree/master/ffnet
It contains all HP Fanfic entries written between 2001-2019 in all available languages. The data doesn't contain the story itself but just the story blurb.
Acknowledgements
The code is entirely mine. The thumbnail and banner are attributed to [Photo by Christian Wagner on Unsplash]
Inspiration
You can answer questions like 'which is the most popular pairing', which language has the most ffs written in it, what has been the general trend like since the last movie or book came out.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43544) of an [OpenML dataset](https://www.openml.org/d/43544). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43544/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43544/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43544/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

